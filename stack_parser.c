#define _XOPEN_SOURCE /* strptime */
#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#include "stack_parser.h"

#define LINE_MAX_WIDTH 10000

#define MALLOC_OR_FATAL(var, size) {var = malloc(size); if(var == NULL) {fprintf(stderr, "Could not allocate memory!\n"); exit(EXIT_FAILURE);}}

/**
 * Extract a string from an entry
 *
 * @param line The line to get information from
 * @param name The name of the string to extract
 * @param dest Destination variable where the string will
 * be stored.
 */
static _Bool extractString(const char *line, const char *name, char **dest){

	//Search the property in the line
	const char *begin = line;
	do {
		begin = strstr(begin, name);

		if(begin == NULL)
			break;

		//Increase beginition size
		begin += strlen(name);

		//Check we won't leak memory
		if(*begin == '\0' || *(begin + 1) == '\0'){
			begin = NULL;
			break;
		}

		//Check if we are at an exceptable beginition
		if(*(begin) == '=' && *(begin + 1) == '"')
			break;
		
	} while(begin != NULL);

	//Check if the begining of the string was not found
	if(begin == NULL)
		return false;

	//Get to the begining of the string
	begin += 2;

	//Determine if the string ends
	const char *end = begin-1;
	do {
		end = strchr(end+1, '"');
	} while(end != NULL && *(end-1) == '\\');

	if(end == NULL)
		return false;

	//Remove braces
	end--;
	
	//We can now allocate memory
	int strlength = end - begin + 1;
	MALLOC_OR_FATAL(*dest, sizeof(char)*(strlength+1));
	strncpy(*dest, begin, strlength);
	(*dest)[strlength] = '\0';

	return true;
}

/**
 * Extract a number from an entry
 *
 * @param line The line to extract information from
 * @param name The name of the property to extract
 * @param *dest Where the int will be written
 * @return TRUE in case of success / FALSE else
 */
static _Bool extractNumber(const char *line, const char *name, long long int *dest){

	//First, extract matching string
	char *string = NULL;
	extractString(line, name, &string);

	//Check if we got a NULL string
	if(string == NULL)
		return false;

	if(strlen(string) == 0)
		*dest = 0;
	else {
		*dest = atoi(string);
	}

	//Free memory
	free(string);

	return true;
}

static _Bool extractTime(const char *line, const char *name, int *dest){

	//First, extract matching string
	char *string = NULL;
	extractString(line, name, &string);

	//Check if we got a NULL string
	if(string == NULL)
		return false;

	if(strlen(string) == 0)
		*dest = 0;
	else {

		string[19] = '\0';
		struct tm time = {0};
		strptime(string, "%Y-%m-%dT%H:%M:%S", &time);
		*dest = timegm(&time);

	}

	//Free memory
	free(string);

	return true;
}

/**
 * Turn a line entry into a post and return it
 *
 * @param line The line to parse
 * @return Generated post object (NULL in case of failure)
 */
static se_parser_post_t* parsePost(const char *line){

	//Allocate memory to target post
	se_parser_post_t *post;
	MALLOC_OR_FATAL(post, sizeof(se_parser_post_t));
	se_parser_post_init(post);

	//Extract post information
	extractNumber(line, "Id", &post->id);
	extractNumber(line, "PostTypeId", &post->postTypeID);
	extractNumber(line, "AcceptedAnswerId", &post->acceptedAnswerID);
	extractNumber(line, "ParentId", &post->parentId);
	extractTime(line, "CreationDate", &post->creationTime);
	extractNumber(line, "Score", &post->score);
	extractNumber(line, "ViewCount", &post->viewCount);
	extractString(line, "Body", &post->body);
	extractNumber(line, "OwnerUserId", &post->ownerUserID);
	extractNumber(line, "LastEditorUserId", &post->lastEditorUserId);
	extractString(line, "LastEditorDisplayName", &post->lastEditorDisplayName);
	extractTime(line, "LastEditDate", &post->lastEditTime);
	extractTime(line, "LastActivityDate", &post->lastActivityTime);
	extractString(line, "Title", &post->title);
	extractString(line, "Tags", &post->tags);
	extractNumber(line, "AnswerCount", &post->answerCount);
	extractNumber(line, "CommentCount", &post->commentCount);
	extractNumber(line, "FavoriteCount", &post->favoriteCount);

	return post;
}

void se_parser_parse_post(FILE *file, se_parser_parse_post_callback cb) {

	//We process only the lines that contains a post
	char line[LINE_MAX_WIDTH+1];
	se_parser_post_t *post;

	//Process each line of the file
	while(fgets(line, LINE_MAX_WIDTH, file) != NULL) {

		//Parse the post (if any)
		if(strstr(line, "<row ") != NULL){
			post = parsePost(line);
			if(post != NULL)
				cb(post);
		}

	}

}

void se_parser_post_init(se_parser_post_t *post){

	//Set all value to 0 by default
	post->id = 0;
	post->postTypeID = 0;
	post->acceptedAnswerID = 0;
	post->parentId = 0;
	post->creationTime = 0;
	post->score = 0;
	post->viewCount = 0;
	post->body = NULL;
	post->ownerUserID = 0;
	post->lastEditorUserId = 0;
	post->lastEditorDisplayName = NULL;
	post->lastEditTime = 0;
	post->lastActivityTime = 0;
	post->title = NULL;
	post->tags = NULL;
	post->answerCount = 0;
	post->commentCount = 0;
	post->favoriteCount = 0;

}

void se_parser_post_free(se_parser_post_t *post){
	if(post->body != NULL) free(post->body);
	if(post->lastEditorDisplayName != NULL) free(post->lastEditorDisplayName);
	if(post->title != NULL) free(post->title);
	if(post->tags != NULL) free(post->tags);
	free(post);
}