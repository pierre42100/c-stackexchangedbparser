CC=gcc
CFLAGS= -Wall -g  `curl-config --cflags`
LIBS= `curl-config --libs` -lpthread
OBJ= main.o stack_parser.o


all: clean stackexchangedbparser


%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS)


stackexchangedbparser: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)


clean:
	rm -f *.o *.a stackexchangedbparser


run: all
	./stackexchangedbparser