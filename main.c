#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <curl/curl.h>


#include "stack_parser.h"

#define SEND_MODE_STRING_SIZE 50000
#define IMPORT_URL "http://127.0.0.1:9200/so_posts/_doc"

/**
 * Show application usage
 */
static void usage(){
	printf("Stack Database Dump parser. Usage:\n"
			"\t-h : Show this help\n"
			"\t-i : Read source file from stdin\n"
			"\t-f : Specify the file to open\n"
			"\t-c : Convert the input file into JSON object ready to be imported into the database.\n"
			"\t-I : Perform the import of a list of posts inside of the database\n\n");
}

static void show_post(se_parser_post_t *post){

	printf("SE POST:\n");
	printf("ID: %lld\n", post->id);
	printf("Post type ID: %lld\n", post->postTypeID);
	printf("Accepted answer ID: %lld\n", post->acceptedAnswerID);
	printf("Parent ID: %lld\n", post->parentId);
	printf("Creation time: %d\n", post->creationTime);
	printf("Score: %lld\n", post->score);
	printf("View Count: %lld\n", post->viewCount);
	printf("Body: %s\n", post->body);
	printf("Owner user id: %lld\n", post->ownerUserID);
	printf("Last editor id: %lld\n", post->lastEditorUserId);
	if(post->lastEditorDisplayName != NULL)
		printf("Last editor display name: %s\n", post->lastEditorDisplayName);
	printf("Last edit time: %d\n", post->lastEditTime);
	printf("Last activity time: %d\n", post->lastActivityTime);
	if(post->title != NULL)
		printf("Last editor display name: %s\n", post->title);
	if(post->tags != NULL)
		printf("Last editor display name: %s\n", post->tags);
	printf("Answer count: %lld\n", post->answerCount);
	printf("Comment count: %lld\n", post->commentCount);
	printf("Favorite count: %lld\n", post->favoriteCount);
	printf("\n\n");

	//Free memory
	se_parser_post_free(post);

}

/**
 * Show parsed post in JSON format
 *
 * @param post Post to parse
 */
static void show_json_post(se_parser_post_t *post){
	printf("{");
	printf("\"id\":%lld,", post->id);
	printf("\"post_type_id\":%lld,", post->postTypeID);
	if(post->acceptedAnswerID > 0)
		printf("\"accepted_answer_id\":%lld,", post->acceptedAnswerID);
	printf("\"parent_id\": %lld,", post->parentId);
	printf("\"creation_time\": %d,", post->creationTime);
	printf("\"score\": %lld,", post->score);
	printf("\"view_count\": %lld,", post->viewCount);
	printf("\"body\": \"%s\",", post->body);
	printf("\"owner_user_id\": %lld,", post->ownerUserID);
	if(post->lastEditorUserId > 0)
		printf("\"last_editor_id\": %lld,", post->lastEditorUserId);
	if(post->lastEditorDisplayName != NULL)
		printf("\"last_editor_display_name\": \"%s\",", post->lastEditorDisplayName);
	if(post->lastEditTime > 0)
		printf("\"last_edit_time\": %d,", post->lastEditTime);
	printf("\"last_activity_time\": %d,", post->lastActivityTime);
	if(post->title != NULL)
		printf("\"post_title\": \"%s\",", post->title);
	if(post->tags != NULL)
		printf("\"tags\": \"%s\",", post->tags);
	printf("\"answer_count\": %lld,", post->answerCount);
	if(post->favoriteCount > 0)
		printf("\"favorite_count\": %lld,", post->favoriteCount);
	printf("\"comment_count\": %lld", post->commentCount);
	printf("}\n");

	//Free memory
	se_parser_post_free(post);
}

/**
 * Open the file located at a specific location
 *
 * @param *path The path to the file
 * @return pointer on the file
 */
FILE *open_file(const char *path){

	//Open file
	FILE *file = fopen(path, "r");

	if(file == NULL){
		printf("Could not open file %s !\n", path);
		exit(EXIT_FAILURE);
	}

	return file;
}


/**
 * Perform the import of pre-processed entries
 *
 * @param file The file to read entry into it
 */
static void perform_import(FILE *file){

	CURL *curl;
	CURLcode res;
	char entry[SEND_MODE_STRING_SIZE];
	long long int count = 0;

	do {

		//Get next input
		fgets(entry, SEND_MODE_STRING_SIZE-1, file);
		if(entry == NULL)
			break;

		curl = curl_easy_init();
		if(!curl){
			fprintf(stderr, "Could not initialize cURL !\n");
			exit(EXIT_FAILURE);
		}

		/* First set the URL that is about to receive our POST. This URL can
	       just as well be a https:// URL if that is what should receive the
	       data. */
	    curl_easy_setopt(curl, CURLOPT_URL, IMPORT_URL);


	    //Add headers
	    struct curl_slist *list = NULL;
	    list = curl_slist_append(list, "Content-Type:application/json");

        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);



	    /* Now specify the POST data */
	    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, entry);


		/* Now run off and do what you've been told! */
		res = curl_easy_perform(curl);
		/* Check for errors */
		if(res != CURLE_OK){
			printf("%lld : %s", count, entry);
			fprintf(stderr, "curl_easy_perform() failed: %s\n",
		          curl_easy_strerror(res));
			printf("A failure occurred while trying to insert an entry. Press Enter to continue or Ctrl+C to quit.");
			getchar();

		}

		/* always cleanup */
		curl_easy_cleanup(curl);

		if(count % 100 == 0)
			printf("%lld\n", count);
	
	} while(entry != NULL);
}

int main(int argc, char **argv){

	char c;
	FILE *file = NULL;
	int close_file = 0;
	int is_import = 0;
	int is_convert = 0;

	while((c = getopt(argc, argv, "hif:cI")) != -1){

		switch(c){

			case 'h':
				usage();
				return EXIT_SUCCESS;

			case 'i':
				file = stdin;
				break;

			case 'f':
				file = open_file(optarg);
				close_file = 1;
				break;

			case 'c':
				is_convert = 1;
				break;


			case 'I':
				is_import = 1;
				break;

		}

	}

	//Initialize cURL
	if(curl_global_init(CURL_GLOBAL_DEFAULT) != 0){
		fprintf(stderr, "Could not initialize cURL library !\n");
		return EXIT_FAILURE;
	}

	//Check if no entry was selected
	if(file == NULL){
		fprintf(stderr, "You did not select a file entry (type -h for more info)!\n");
		return EXIT_FAILURE;
	}

	//Process requested action
	if(is_import)
		//Perform import
		perform_import(file);

	//Convert entry
	else if(is_convert)
		se_parser_parse_post(file, show_json_post);

	//Default action : parse posts
	else
		//Parse entries
		se_parser_parse_post(file, show_post);

	//Close file
	if(close_file)
		fclose(file);

	//Close cURL
	curl_global_cleanup();

	return EXIT_SUCCESS;
}
