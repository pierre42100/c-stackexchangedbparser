/**
 * Stack parser - parse list of posts in XML format
 *
 * @author Pierre HUBERT
 */

#pragma once

#include <stdio.h>

/**
 * Single post information
 */
typedef struct se_parser_post_t {

	long long int id;
	long long int postTypeID;
	long long int acceptedAnswerID;
	long long int parentId;
	int creationTime;
	long long int score;
	long long int viewCount;
	char *body;
	long long int ownerUserID;
	long long int lastEditorUserId;
	char *lastEditorDisplayName;
	int lastEditTime;
	int lastActivityTime;
	char *title;
	char *tags;
	long long int answerCount;
	long long int commentCount;
	long long int favoriteCount;


} se_parser_post_t;

/**
 * Callback called each time a new post is decoded
 */
typedef void(*se_parser_parse_post_callback) (se_parser_post_t *);

/**
 * Parse the list of posts of a file
 *
 * @param cb Callback function called each time a new
 * entry is decoded
 *
 * Warning ! It the responsiblity of the callback to free memory !!!
 */
void se_parser_parse_post(FILE *file, se_parser_parse_post_callback cb);

/**
 * Initialize / reset post structure (make sure to avoid memory leak)
 *
 * @param post The post object to initialize
 */
void se_parser_post_init(se_parser_post_t *post);

/**
 * Release memory handled by a StackExchange post
 *
 * @param post The post to free
 */
void se_parser_post_free(se_parser_post_t *post);